package com.fauzi.crud.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="product")
public class Product {

    @Id
    @Column(name = "productid")
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer productId;

    @Column(name = "productname")
    private String productName;

    @Column(name = "supplierid")
    private Integer supplierID;

    @Column(name = "categoryid")
    private Integer categoryID;

    @Column(name = "unit")
    private String unit;

    @Column(name = "price")
    private Integer price;


}
