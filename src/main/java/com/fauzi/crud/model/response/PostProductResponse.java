package com.fauzi.crud.model.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostProductResponse {
    private Integer id;

    private String productName;

    private Integer supplierID;

    private Integer categoryID;

    private String unit;

    private Integer price;
}
