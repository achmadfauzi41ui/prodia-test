package com.fauzi.crud.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PostProductRequest {

    private String productName;

    private Integer supplierID;

    private Integer categoryID;

    private String unit;

    private Integer price;
}
