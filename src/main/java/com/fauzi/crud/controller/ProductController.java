package com.fauzi.crud.controller;

import com.fauzi.crud.model.request.PostProductRequest;
import com.fauzi.crud.model.response.GetProductByIdResponse;
import com.fauzi.crud.model.response.PostProductResponse;
import com.fauzi.crud.service.GetProductByIdService;
import com.fauzi.crud.service.PostProductService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {
    private GetProductByIdService getProductByIdService;
    private PostProductService postProductService;

    public ProductController(GetProductByIdService getProductByIdService, PostProductService postProductService) {
        this.getProductByIdService = getProductByIdService;
        this.postProductService = postProductService;
    }

    @GetMapping(value = "/retrieve/{id}")
    public GetProductByIdResponse getProductById(@PathVariable Integer id){
        return getProductByIdService.execute(id);
    }

    @PostMapping(value = "/save")
    public PostProductResponse postProductResponse(@RequestBody PostProductRequest request){
        return postProductService.execute(request);
    }
}
