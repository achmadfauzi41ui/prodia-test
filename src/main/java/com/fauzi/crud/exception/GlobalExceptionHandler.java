package com.fauzi.crud.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(GeneralException.class)
    public ResponseEntity<ErrorResponse> getGeneralExceptionResponse(GeneralException ex){
        return new ResponseEntity<>(ErrorResponse.builder()
                .errorCode(ex.getErrorCode())
                .errorMessage(ex.errorDesc)
                .build(), ex.getHttpStatus());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> getGeneralExceptionResponse(Exception ex){
        return new ResponseEntity<>(ErrorResponse.builder()
                .errorCode("5000")
                .errorMessage("Unknown Error from Server")
                .build(),new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> getGeneralExceptionResponse(){
        return new ResponseEntity<>(ErrorResponse.builder()
                .errorCode("4000")
                .errorMessage("Bad Request From Client request")
                .build(),new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

}
