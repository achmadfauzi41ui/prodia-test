package com.fauzi.crud.service;

import com.fauzi.crud.model.entity.Product;
import com.fauzi.crud.model.request.PostProductRequest;
import com.fauzi.crud.model.response.PostProductResponse;
import com.fauzi.crud.repository.ProductRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PostProductService {
    private ProductRepository productRepository;

    public PostProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public PostProductResponse execute(PostProductRequest request){
        Product product = Product.builder()
                .productName(request.getProductName())
                .supplierID(request.getSupplierID())
                .categoryID(request.getCategoryID())
                .price(request.getPrice())
                .unit(request.getUnit())
                .build();

        productRepository.save(product);

        return PostProductResponse.builder()
                .id(product.getProductId())
                .productName(request.getProductName())
                .supplierID(request.getSupplierID())
                .categoryID(request.getCategoryID())
                .price(request.getPrice())
                .unit(request.getUnit())
                .build();
    }
}
