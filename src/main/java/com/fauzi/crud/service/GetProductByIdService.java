package com.fauzi.crud.service;

import com.fauzi.crud.exception.GeneralException;
import com.fauzi.crud.model.entity.Product;
import com.fauzi.crud.model.response.GetProductByIdResponse;
import com.fauzi.crud.repository.ProductRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class GetProductByIdService {
    private ProductRepository productRepository;

    public GetProductByIdService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public GetProductByIdResponse execute(Integer id){
        Product product = productRepository.findById(id).orElseThrow(()->{
            log.error("Can't find User By Id with request: {}", id);
            throw new GeneralException(HttpStatus.CONFLICT, "4009","Can't find user by id request");
        });
        return GetProductByIdResponse.builder()
                .id(product.getProductId())
                .productName(product.getProductName())
                .supplierID(product.getSupplierID())
                .categoryID(product.getCategoryID())
                .price(product.getPrice())
                .unit(product.getUnit())
                .build();
    }
}
