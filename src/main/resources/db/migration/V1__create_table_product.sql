CREATE TABLE Product (
    productid int(20) AUTO_INCREMENT,
    productname varchar(100),
	supplierid int(20),
	categoryid int(20),
    unit varchar(100),
	price int(30),
	CONSTRAINT product_pk1 primary key(productid)
);